covid lab

dorm with most positive:
select count(test_result), dorm from master_data where test_result = 1 group by dorm;
+--------------------+------------+
| count(test_result) | dorm       |
+--------------------+------------+
|                 84 | Alspaugh   |
|                660 | Off Campus |
|                 76 | Pegram     |
|                 95 | House CC   |
|                112 | Jarvis     |
|                 99 | House A    |
|                115 | Epworth    |
|                114 | G-A        |
+--------------------+------------+

class year took most tests:
mysql> select count(test_date), class from master_data where test_date is not null group by class;
+------------------+-------+
| count(test_date) | class |
+------------------+-------+
|              853 |  2020 |
|              829 |  2023 |
|              843 |  2022 |
|              861 |  2021 |
+------------------+-------+
4 rows in set (0.00 sec)

took test but did not return:
mysql> select count(userid) from master_data where test_date is not null and test_returned is null;
+---------------+
| count(userid) |
+---------------+
|           687 |
+---------------+
1 row in set (0.00 sec)


break up tables:

new dorm table
create table dorm(id int primary key auto_increment);

alter table dorm add column name varchar(25) not null;

insert into dorm (name) select distinct dorm from master_data;

Set students dorm field to correct dorm:

mysql> update students set dorm = (select dorm.id from master_data inner join dorm on master_data.dorm=dorm.name where students.userid=master_data.userid);
Query OK, 15000 rows affected (1 min 50.33 sec)
Rows matched: 15000  Changed: 15000  Warnings: 0

mysql> select * from students limit 10;
+---------+------------+-----------+----+------+
| userid  | firstname  | lastname  | id | dorm |
+---------+------------+-----------+----+------+
| 4201200 | Kaylene    | Nakazibwe |  1 |    1 |
| 4201201 | Bilyana    | McMahon   |  2 |    2 |
| 4201202 | Yuri       | Crossey   |  3 |    3 |
| 4201203 | Joie       | Knuffman  |  4 |    4 |
| 4201204 | Evangaline | Spiritos  |  5 |    5 |
| 4201205 | Yumei      | Tarpeh    |  6 |    5 |
| 4201206 | Kendrick   | Cribb     |  7 |    5 |
| 4201207 | Mone       | Kephart   |  8 |    6 |
| 4201208 | Tywan      | Ando      |  9 |    7 |
| 4201209 | Liming     | Pegram    | 10 |    5 |
+---------+------------+-----------+----+------+
10 rows in set (0.00 sec)
