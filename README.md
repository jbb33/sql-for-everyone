# SQL for Everyone

Instructions and examples for learning SQL

### Trying SQL

[W3Schools](https://www.w3schools.com/sql/sql_syntax.asp)

[SQLShare](https://itconnect.uw.edu/learn/tools/sqlshare/)

[Virtual Computing manager](https://vcm.duke.edu/)

[Oracle SQL Developer](https://www.oracle.com/tools/downloads/sqldev-v192-downloads.html)

[SQLite Studio](https://sqlitestudio.pl/features/)

### Example Data

[IMDB](https://datasets.imdbws.com/)

[Census Data](https://www.census.gov/data/tables.html)

[MIDS/Colab Example Data](https://gitlab.oit.duke.edu/jbb33/sql-for-everyone/-/raw/master/student_contacts.sql)

### Learning SQL
Reserved Words

[W3Schools Reserved Words](https://www.w3schools.com/Sql/sql_ref_keywords.asp​)

[Actian psql docs Reserved Words](https://docs.actian.com/psql/psqlv13/index.html#page/sqlref%2Fsqlkword.htm%23​)

Data Types
[W3Schools Data Types](https://www.w3schools.com/sql/sql_datatypes.asp)

Funcions and Operators
[dev.myqsl.com](https://dev.mysql.com/doc/refman/5.7/en/functions.html)


### Setting up data
Get your own Ubuntu VM or use your local machine or [reserve a VM via the VCM](https://vcm.duke.edu/).
```sh
$ sudo apt-get install mysql-server
$ sudo -i #BAD unless example​
$ mysql -u root​
```
```sql
CREATE DATABASE yourdbname; /* Create a daatbase to improt example data */​
quit;
```
```sh
# grab example data, import it into your newly created db
$ wget https://gitlab.oit.duke.edu/jbb33/sql-for-everyone/-/raw/master/student_contacts.sql
$ mysql -u root yourdbname < student_contacts.sql​
$ mysql -u root​
```
```sql
USE yourdbname;
SELECT * FROM master_data;
```
You should create a password for root user and create a user for your application with appropriate privileges.

### Example Queries all based upon fake data set provided for mids course
```sql
/* Use database */
USE yourdbname;

/* Show tables */
SHOW TABLES;

/* Describe Table */
DESCRIBE master_data;

/* More detail about tables */
SHOW COLUMNS FROM master_data;
SHOW FULL COLUMNS FROM master_data;

/* Give me everything */
SELECT * FROM master_data;
SELECT * FROM master_data LIMIT 10;

/* Per dorm count of students */
SELECT Count(userid), dorm FROM master_data GROUP BY dorm;

/* With AS */
SELECT COUNT(userid) AS Students, dorm AS Dorm FROM master_data GROUP BY Dorm;

/* How many test for each test date using subquery */
select test_date AS Date, COUNT(test_date) AS Tests FROM master_data WHERE test_date IS NOT NULL group by Date;

/* Which test dates had more than 30 tests? */
select Date, Tests FROM (SELECT test_date AS Date, COUNT(test_date) AS Tests FROM master_data group by Date) as date WHERE Tests > 30;

/* Find all engineers, count them, and group by degree */
select major, count(*) from master_data where major LIKE '%Engineering%' group by major;

/* Change database by replacing NULL dorms with "Off Campus" */
update master_data set dorm = "Off Campus" where dorm IS NULL;

/* Create table based upon select result */
create table covid_tests as select userid, test_date, test_returned, test_result from master_data where test_date IS NOT NULL;

/* Add a primary auto incrementing key */
ALTER TABLE covid_tests ADD id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

/* Inner join data from two tables */
select students.userid, students.firstname, students.lastname, covid_tests.test_date FROM students INNER JOIN covid_tests on students.userid=covid_tests.userid;

/* Left join same data */
select students.userid, students.firstname, students.lastname, covid_tests.test_date FROM students LEFT JOIN covid_tests on students.userid=covid_tests.userid;
```
