Grab data from repo
wget https://gitlab.oit.duke.edu/jbb33/sql-for-everyone/-/raw/master/lab_lego_dataset.sql
mysql -u root yourdbname < lab_lego_dataset.sql

LEGO lab queries - examine tables and examine diagram: sometimes relations are not clear, diagrams help understanding

INNER JOIN 
total: 11673
-------------
SELECT sets.set_num, sets.name, themes.name FROM sets INNER JOIN themes ON sets.theme_id = themes.id ORDER BY sets.name DESC;
 
LEFT JOIN

total: 11673

-----------

SELECT sets.set_num, sets.name, themes.name FROM sets LEFT JOIN themes ON sets.theme_id = themes.id ORDER BY sets.name DESC;


RIGHT JOIN

total: 11712

-------------

SELECT sets.set_num, sets.name, themes.name FROM sets RIGHT JOIN themes ON sets.theme_id = themes.id ORDER BY sets.name DESC;

GUI SHOWCASE
good stuff

bad stuff: must connect to remote host (tunnel, firewalls), accidentally changing data